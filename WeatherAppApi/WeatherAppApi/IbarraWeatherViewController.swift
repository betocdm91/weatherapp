//
//  IbarraWeatherViewController.swift
//  WeatherAppApi
//
//  Created by Nelson Chicaiza on 29/5/18.
//  Copyright © 2018 Carlos Osorio. All rights reserved.
//

import UIKit

class IbarraWeatherViewController: UIViewController {
    
    
 
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonPressedBuscar(_ sender: Any) {
        let urlstring = "http://api.openweathermap.org/data/2.5/weather?q=\(cityTextField.text ?? "quito")&appid=de79d877370312eb9242162107a007fe"
        
        let url = URL(string: urlstring)
        let sesion = URLSession.shared
        let task = sesion.dataTask(with: url!) { (data, response, error) in
            guard let data = data else {
            print("Error No data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(NelsonWeatherInfo.self, from:
                data) else {
                print("Error decoding Weather")
                return
            }
            
            //self.resultLabel.text = "\(weatherInfo.weather[0].description)"
            //print(weatherInfo.weather[0].description)
            DispatchQueue.main.async {
                self.resultLabel.text = "\(weatherInfo.weather[0].description)"
            }
            
        }
        
        task.resume() //se manda a ejecutar la tarea
    }
    

}
