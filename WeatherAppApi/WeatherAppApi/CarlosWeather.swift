//
//  CarlosWeather.swift
//  WeatherAppApi
//
//  Created by Carlos Osorio on 30/5/18.
//  Copyright © 2018 Carlos Osorio. All rights reserved.
//

import Foundation

/*
 let mainWeather:String
 
 enum CodingKeys: String, CodingKey {
 case weather
 case mainWeather = "main_weather"
 }
 */

struct CarlosWeatherInfo: Decodable{
    let weather: [CarlosWeather]

}

struct CarlosWeather: Decodable {
    let id:Int
    let description:String
}
