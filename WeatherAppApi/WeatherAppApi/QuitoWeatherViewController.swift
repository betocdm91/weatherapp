//
//  QuitoWeatherViewController.swift
//  WeatherAppApi
//
//  Created by Carlos Osorio on 29/5/18.
//  Copyright © 2018 Carlos Osorio. All rights reserved.
//

import UIKit

class QuitoWeatherViewController: UIViewController {

    
    @IBOutlet weak var cityTextField: UITextView!
    @IBOutlet weak var resultLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func ButtonPressedBuscar(_ sender: Any) {
        let urlstring = "http://api.openweathermap.org/data/2.5/weather?q=\(cityTextField.text ?? "quito")&appid=905c4bdb806ee19950c572168a128e46"
        let url = URL(string: urlstring)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response, error) in
            guard let data = data else {
                print("Error No data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(CarlosWeatherInfo.self, from: data) else{
                print("Error decoding weather")
                return
            }
            
            DispatchQueue.main.async {
                self.resultLabel.text = "\(weatherInfo.weather[0].description)"
            }
            
        }
        task.resume()
    }
}
