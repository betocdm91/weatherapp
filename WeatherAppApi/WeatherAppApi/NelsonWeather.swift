//
//  NelsonWeather.swift
//  WeatherAppApi
//
//  Created by Nelson Chicaiza on 30/5/18.
//  Copyright © 2018 Carlos Osorio. All rights reserved.
//

import Foundation

/*
 "main_weather": "rain"
 enum Coding
 */

struct NelsonWeatherInfo: Decodable {
    let weather: [NelsonWeather]

}

struct NelsonWeather: Decodable {
    let id:Int
    let description:String
    
    
}
